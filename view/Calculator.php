<?php
require_once ("../src/function.php");
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Calculator</title>
    <link href="../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="../resource/bootstrap/js/bootstrap.min.js"></script>
    <style>
        .pagination{

            margin: 0;
        }

        .table-striped > tbody > tr:nth-child(odd) > td
        {
            background-color: deepskyblue;

        }
        .table-striped > tbody > tr:nth-child(even) > td
        {
            background-color: #bce8f1;
        }
    </style>
</head>
<body>
<form action="Calculator.php" method="post">
    <table class="table table-bordered table-striped" border="4">
        <h1 class="alert-info" align="center">Calculator</h1>
        <tr>
            <td>Enter the first Number : </td>
            <td><input type="number" name ="num1"/></td>
        </tr>

        <tr>
            <td>Enter the second Number : </td>
            <td><input type="number" name ="num2"/></td>
        </tr>

        <tr>
            <td></td>
            <td><input type="submit" name="calculation"
                       value="Calculate" /></td>
        </tr>

    </table>
</form>
</body>
</html>
<?php
if(isset($_POST['calculation'])){
    $numOne = $_POST['num1'];
    $numTwo = $_POST['num2'];

    if(empty($numOne) or empty($numTwo)){
        echo "<span style='color:#EE6554'>Field must
           not be empty.</span><br>";
    } else {
        echo "First Number is: ".$numOne. " Second Number
          is ".$numTwo."<br/>";
        $cal = new Calculation;
        $cal->add($numOne, $numTwo);
        $cal->sub($numOne, $numTwo);
        $cal->mul($numOne, $numTwo);
        $cal->div($numOne, $numTwo);

    }
}
?>